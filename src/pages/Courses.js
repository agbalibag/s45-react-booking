import { Fragment, useEffect, useState } from 'react'
import Course from '../components/Course'

//Bootstrap Components
import Container from 'react-bootstrap/Container'

//Data Imports
// import courses from '../mock-data/courses'

const Courses = () => {
	//State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState()

	//Retrieves  the courses from the database upon initial render of the "Courses"
	useEffect(()=>{
		fetch('http://localhost:4000/api/courses')
			.then(res => res.json())
			.then(data =>{
				console.log(data)
				setCourses(data.map(course=>{
					return(
						<CourseCard key={course._id} courseProp={course} />
					);
				}));

			})
	}, [])




	return(
		<Container fluid>
			{CourseCards}
		</Container>
		)
}

export default Courses