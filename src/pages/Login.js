//Base Imports
import {useState, useContext} from 'react'
import {Navigate} from 'react-router-dom';
//Bootstrap Components
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'

//App Imports
import UserContext from '../UserContext';

const Login = () => {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('')
	const {user, setUser} = useContext(UserContext)

function login(e) {
		e.preventDefault();
		alert('You are now logged in.');

		localStorage.setItem('email',email)

		setUser({email:email})
		console.log(user);

		setEmail('');
		setPassword('');

	}
		if(user.email !== null){
		return <Navigate to="/" />
	}

	return (
		<Container fluid>
			<h3>Login</h3>
			<Form onSubmit = {login}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required value = {email} onChange = {(e)=> setEmail(e. target.value)}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value = {password} onChange = {(e)=> setPassword(e. target.value)}/>
				</Form.Group>
				<Button variant="success" type="submit">Login</Button>
			</Form>
		</Container>
		);
}

export default Login;