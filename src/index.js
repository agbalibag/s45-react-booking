import React from 'react';

//Base Imports
import ReactDOM from 'react-dom';
import{ BrowserRouter, Route, Routes } from 'react-router-dom'

import AppNavBar from './components/AppNavBar';
// import Course from './components/Course'

//CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'

//Bootstrap Components
import Container from 'react-bootstrap/Container';

//Page Components
// import Home from './pages/Home';
// import Courses from './pages/Courses';
// import Counter from './components/Counter';
// import Register from './pages/Register';
// import Login from './pages/Login';

import App from "./App";


ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,

  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

/*
import React from 'react';
import ReactDOM from 'react-dom';
import AppNavBar from './components/AppNavBar';
import Course from './components/Course'

//CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'

//Bootstrap Components
import Container from 'react-bootstrap/Container';

//Page Components
import Home from './pages/Home';
import Courses from './pages/Courses';
*/