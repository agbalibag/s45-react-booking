//Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import PageNotFound from './components/PageNotFound';
//App Imports
import UserContext from './UserContext';

const App = () => {
  const [user, setUser] = useState({ email: localStorage.getItem('email') })

  const unsetUser = () => {
    localStorage.clear()
    setUser({email:null})
  }

	return (
    <UserContext.Provider value = {{user, setUser, unsetUser}}>
	     <BrowserRouter>
          <AppNavBar />
            <Routes>
      	     <Route path = "/" element = {<Home />} />
              <Route path = "/courses" element = {<Courses />} />
              <Route path = "/register" element = {<Register />} />
              <Route path = "/login" element = {<Login />} />
              <Route element = {<PageNotFound />} />
            </Routes>
        </BrowserRouter>
    </UserContext.Provider>  
		);
}

export default App;