// Base Imports
import React, {useContext, Fragment} from 'react'
import { Link, NavLink, useNavigate } from 'react-router-dom';

//Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

//App Imports
import UserContext from '../UserContext';

const AppNavBar = () => {
  const { user, unsetUser } = useContext(UserContext)
  const history = useNavigate;

  const logout = () => {
    unsetUser()
    history.push('/login');
  }

  // let age = 18;
  // let message;
  // message = age >= 16 ? 'You can drive.' : 'You cannot drive.';
  // condition ? expressionIfTrue : expressionIfFalse;

  let rightNav = (user.email === null) ? (
      <Fragment>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
      </Fragment>

    ) : (
      <Fragment>
            <Nav.Link as={NavLink} onClick = {logout} to="/logout">Logout</Nav.Link>
      </Fragment>
    );

	return(
        <Navbar bg="light" expand="lg">
            <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    {/* <Nav.Link as={NavLink} to="/">Home</Nav.Link> */}
                    {/* <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link> */}
                    {/* <Nav.Link as={NavLink} to="/register">Register</Nav.Link> */}
                    {/* <Nav.Link as={NavLink} to="/login">Login</Nav.Link> */}

                    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                </Nav>
                <Nav className="ml-auto">
                    {rightNav}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
		)
}

export default AppNavBar